# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'safarihistory_ui.ui'
#
# Created: Thu Feb 21 12:01:59 2013
#      by: pyside-uic 0.2.14 running on PySide 1.1.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_SafariHistory(object):
    def setupUi(self, SafariHistory):
        SafariHistory.setObjectName("SafariHistory")
        SafariHistory.resize(683, 495)
        self.verticalLayout = QtGui.QVBoxLayout(SafariHistory)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_5 = QtGui.QLabel(SafariHistory)
        self.label_5.setFrameShape(QtGui.QFrame.Panel)
        self.label_5.setFrameShadow(QtGui.QFrame.Raised)
        self.label_5.setObjectName("label_5")
        self.verticalLayout.addWidget(self.label_5)
        self.historyTree = QtGui.QTreeWidget(SafariHistory)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.historyTree.sizePolicy().hasHeightForWidth())
        self.historyTree.setSizePolicy(sizePolicy)
        self.historyTree.setObjectName("historyTree")
        self.verticalLayout.addWidget(self.historyTree)
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtGui.QLabel(SafariHistory)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label)
        self.label_2 = QtGui.QLabel(SafariHistory)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_2)
        self.label_3 = QtGui.QLabel(SafariHistory)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_3)
        self.label_4 = QtGui.QLabel(SafariHistory)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.label_4)
        self.label_url = QtGui.QLineEdit(SafariHistory)
        self.label_url.setReadOnly(True)
        self.label_url.setObjectName("label_url")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.label_url)
        self.label_title = QtGui.QLineEdit(SafariHistory)
        self.label_title.setReadOnly(True)
        self.label_title.setObjectName("label_title")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.label_title)
        self.label_visitcount = QtGui.QLineEdit(SafariHistory)
        self.label_visitcount.setReadOnly(True)
        self.label_visitcount.setObjectName("label_visitcount")
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.label_visitcount)
        self.label_redirurl = QtGui.QTextEdit(SafariHistory)
        self.label_redirurl.setMaximumSize(QtCore.QSize(16777215, 50))
        self.label_redirurl.setReadOnly(True)
        self.label_redirurl.setObjectName("label_redirurl")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.label_redirurl)
        self.verticalLayout.addLayout(self.formLayout)

        self.retranslateUi(SafariHistory)
        QtCore.QMetaObject.connectSlotsByName(SafariHistory)

    def retranslateUi(self, SafariHistory):
        SafariHistory.setWindowTitle(QtGui.QApplication.translate("SafariHistory", "Safari History Browser", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("SafariHistory", "Right click on list to copy link or open in browser", None, QtGui.QApplication.UnicodeUTF8))
        self.historyTree.headerItem().setText(0, QtGui.QApplication.translate("SafariHistory", "ID", None, QtGui.QApplication.UnicodeUTF8))
        self.historyTree.headerItem().setText(1, QtGui.QApplication.translate("SafariHistory", "Last visit", None, QtGui.QApplication.UnicodeUTF8))
        self.historyTree.headerItem().setText(2, QtGui.QApplication.translate("SafariHistory", "Title/URL", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("SafariHistory", "URL", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("SafariHistory", "Title", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("SafariHistory", "Redirect URLs", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("SafariHistory", "Visit count", None, QtGui.QApplication.UnicodeUTF8))

