# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'thumbsbrowser_ui.ui'
#
# Created: Thu Feb 21 12:01:59 2013
#      by: pyside-uic 0.2.14 running on PySide 1.1.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_ThumbsBrowser(object):
    def setupUi(self, ThumbsBrowser):
        ThumbsBrowser.setObjectName("ThumbsBrowser")
        ThumbsBrowser.resize(490, 453)
        self.verticalLayout = QtGui.QVBoxLayout(ThumbsBrowser)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtGui.QLabel(ThumbsBrowser)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.thumbsFilesList = QtGui.QComboBox(ThumbsBrowser)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.thumbsFilesList.sizePolicy().hasHeightForWidth())
        self.thumbsFilesList.setSizePolicy(sizePolicy)
        self.thumbsFilesList.setObjectName("thumbsFilesList")
        self.horizontalLayout.addWidget(self.thumbsFilesList)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.buttonLeft = QtGui.QPushButton(ThumbsBrowser)
        self.buttonLeft.setObjectName("buttonLeft")
        self.horizontalLayout_2.addWidget(self.buttonLeft)
        self.descriptionLabel = QtGui.QLabel(ThumbsBrowser)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.descriptionLabel.sizePolicy().hasHeightForWidth())
        self.descriptionLabel.setSizePolicy(sizePolicy)
        self.descriptionLabel.setText("")
        self.descriptionLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.descriptionLabel.setObjectName("descriptionLabel")
        self.horizontalLayout_2.addWidget(self.descriptionLabel)
        self.buttonRight = QtGui.QPushButton(ThumbsBrowser)
        self.buttonRight.setObjectName("buttonRight")
        self.horizontalLayout_2.addWidget(self.buttonRight)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.thumbsTable = QtGui.QTableWidget(ThumbsBrowser)
        self.thumbsTable.setVerticalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
        self.thumbsTable.setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
        self.thumbsTable.setRowCount(100)
        self.thumbsTable.setColumnCount(5)
        self.thumbsTable.setObjectName("thumbsTable")
        self.thumbsTable.setColumnCount(5)
        self.thumbsTable.setRowCount(100)
        self.verticalLayout.addWidget(self.thumbsTable)

        self.retranslateUi(ThumbsBrowser)
        QtCore.QMetaObject.connectSlotsByName(ThumbsBrowser)

    def retranslateUi(self, ThumbsBrowser):
        ThumbsBrowser.setWindowTitle(QtGui.QApplication.translate("ThumbsBrowser", "Thumbnails Browser", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("ThumbsBrowser", "Available thumbnail files:", None, QtGui.QApplication.UnicodeUTF8))
        self.buttonLeft.setText(QtGui.QApplication.translate("ThumbsBrowser", "<", None, QtGui.QApplication.UnicodeUTF8))
        self.buttonRight.setText(QtGui.QApplication.translate("ThumbsBrowser", ">", None, QtGui.QApplication.UnicodeUTF8))

